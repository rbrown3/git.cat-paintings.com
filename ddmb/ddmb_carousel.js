// =============================================================================
// ddmb_carousel - requires jquery


// ddmb_carousel "class"
function ddmb_carousel(id, display_time_ms)
{

   var t = 1000; // (display_time_ms === undefined) ? 10000 : display_time_ms
   var div_id = "#" + "ddmb_carousel";
   var countdown_id = "#" + "ddmb_countdown";
   var left = 0;
   var num_img = 6;
   var thumb_file_url = '/thumbs.txt';

  // alert ("inner2 = " + inner2); 

   var timer;
   var countdown = 0;

   // ==========================================================================
   // Start the carousel after the thumb file has been read 
   
   function start_carousel() {
      timer = setInterval(function() {
         countdown = (countdown+1)%10;
         var img_name = '/images/ddmb_countdown/' + countdown + '.jpg';
         if (countdown==0) {
	    var html_str = '';
            for (var i = 0; i < num_img; i++) {
	       var random_index = Math.floor(Math.random() * thumb_array.length);
	       var img_url = thumb_array[random_index];
	       var anchor = img_url.replace(/[^\/]+$/, '');
	       html_str += '<a href="' + anchor + '"><IMG src="' + img_url + '"></a>' + "\n";
	    }
	    // alert ("html_str =\n" + html_str);
            // $(div_id).fadeOut("slow" , "swing").html(html_str).fadeIn("slow" , "swing");
            $(div_id).fadeOut("slow" , "swing" , function() { $(this).html(html_str).fadeIn("slow" , "swing");  });

//            left -= 650;
//            if (left < -80 * 100) left = 0;
//            $(div_id).fadeOut("slow" , "swing").animate({'marginLeft' : left+"px"}).fadeIn("slow" , "swing");
         } 
      } , 
      t ); 
   };


//   start();


   // ==========================================================================
   // Read server file that contains the thumb urls and then start carousel
   
   $.get(thumb_file_url,function(data,status) {
      thumb_array = data.split("\n");
      start_carousel();
   });        

} // end ddmb_carousel "class"

// var c1 = new ddmb_carousel("ddmb_slideshow");

// end ddmb_slideshow
// =============================================================================

