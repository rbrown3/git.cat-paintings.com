// =============================================================================
// ddmb_slideshow - requires jquery


// ddmb_slideshow "class"
function ddmb_slideshow(slideshow_id, display_time_ms, transition_time_ms , images_txt)
{

   var ssid = "ddmb_slideshow";  // (slideshow_id === undefined) ? "ddmb_slideshow3" : slideshow_id;
   var dtms = 3000; // (display_time_ms === undefined) ? 5000 : display_time_ms
   var ttms = (transition_time_ms === undefined) ? 2500 : transition_time_ms;

   var image_file_url   = (images_txt === undefined) ? "/images.txt" : images_txt;
   var div_id           = ssid;
   var image0           = "img" + ssid + "0";
   var image1           = "img" + ssid + "1";
 
   var a0               = "a" + ssid + "0";
   var a1               = "a" + ssid + "1";
 
   var inner = $( "#" + div_id ).html(); // should just be the img
   var inner2 = inner + inner;
   $("#" + div_id ).html(inner2); // write 2 replicated images
   $("#" + div_id + " img:eq(0)").attr("id",image0); // add id to first img
   $("#" + div_id + " img:eq(1)").attr("id",image1); // add id to second img
   $("#" + div_id + " a:eq(0)").attr("id",a0); // add id to first img
   $("#" + div_id + " a:eq(1)").attr("id",a1); // add id to first img

  // alert ("inner2 = " + inner2); 
   
   var image_cnt        = 0;
   var image_url_array  = new Array();

   var timer;

   // ==========================================================================
   // Start the timer
   
   function start() {
      timer = setInterval(function() {
          var random_index = Math.floor(Math.random() * image_url_array.length);
          var img_src = image_url_array[random_index];
          var a_href = img_src.replace( /\/[^\/]+$/, "/" );
          if (image_cnt % 2 == 0) {
             $("#" + image1).fadeOut(ttms, function() { 
                $("#" + image1).attr("src",img_src);
                $("#" + a1).attr("href", a_href );
             } );
          } else {
             $("#" + image1).fadeIn(ttms, function() { 
                $("#" + image0).attr("src",img_src); 
                $("#" + a0).attr("href", a_href );
             } );
          }
          image_cnt++;
      } , 
      dtms ); // change image every few seconds
   };


   // ==========================================================================
   // Read server file that contains the image urls
   
   $.get(image_file_url,function(data,status) {
      image_url_array = data.split("\n");
      start();
   });        

} // end ddmb_slideshow "class"

// var s1 = new ddmb_slideshow("ddmb_slideshow");

// end ddmb_slideshow
// =============================================================================

